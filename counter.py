#!/usr/bin/env python

import inkex
from inkex import TextElement, load_svg
from lxml import etree


class Counter(inkex.Effect):

    def add_arguments(self, pars):
        pars.add_argument("--prefix", type=str, default="PBQ-", help="Prefix")
        pars.add_argument("--digits", type=int, default=5,      help="Number of digits")
        pars.add_argument("--amount", type=int, default=5,      help="Number of elements")

    def effect(self):
        layer = self.svg.get_current_layer()
        pagewidth = self.get_pagewidth()
        gap = 10

        for i in range(1, self.options.amount):
            message = self.options.prefix + str(i).zfill(self.options.digits)
            x = (pagewidth + gap) * i
            y = 0

            layer.add(self.add_text(x, y, message))

    def add_text(self, x, y, text):
        elem = TextElement(x=str(x), y=str(y))
        elem.text = str(text)
        elem.style = {
            'font-size': self.svg.unittouu('14pt'),
            'fill-opacity': '1.0',
            'stroke': 'none',
            'font-weight': 'normal',
            'font-style': 'normal'
        }
        return elem

    def get_content(self):
        # TODO: Select everything in the current layer and return it to a variable
        pass

    def add_page(self, width, gutter):
        # TODO 1: Add page RECT to expected location
        # TODO 2: Add page element to SVG element
        # +->  <inkscape:page x="0" y="0" width="210" height="297" id="page203" />
        # TODO 3: Add parsed content to page
        pass

    def parse_variable(self, page, text):
        # TODO: FIND (all) TEXTELEMENT(s) WITH LABEL "COUNTER" (case insensitive) and replace by text value
        pass

    def get_pagewidth(self):
        # Getting page width from SVG attributes
        return self.svg.attrib['width']

    def get_pageheight(self):
        # Getting page height from SVG attributes
        return self.svg.attrib['height']

if __name__ == '__main__':
    Counter().run()
